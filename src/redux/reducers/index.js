import { combineReducers } from 'redux';
import productReducer from './productReducer.js';

export default combineReducers({
	productReducer
});