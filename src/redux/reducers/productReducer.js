const INITIAL_STATE = {
	productList:[],

	columns:[
		{
			title:'ID',
			field:'id'
		},
		{
			title:'Name',
			field:'name'
		},
		{
			title:'Price',
			field:'price',
			type:'numeric'
		},
		{
			title:'QR Code',
			field:'qrCode'
		},
		{
			title:'STOCK',
			field:'stock',
			type:'numeric'
		}
	]
};

export default (state=INITIAL_STATE, action) => {
	switch (action.type) {
		case 'setProductList':
			return {
				...state,
				productList: action.payload,
			}

		default:
			return state;
	}
}