export const setProducts = (value) => (dispatch) => {
	dispatch({
		type: 'setProductList',
		payload: value,
	});
};