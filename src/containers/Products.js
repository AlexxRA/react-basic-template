import React, {Component} from 'react';
import { connect } from 'react-redux';
import * as productActions from '../redux/actions/productActions'
import axios from 'axios'
import {BASE_API_URL} from '../settings'
import MaterialTable from 'material-table'

class Products extends Component {

    constructor(props) {
        super(props);
        console.log(this.props);
    }
    
    async componentDidMount(){

        const productList = this.props.productList;
        if(!productList.length){
            try {
                const result = await axios.get(`${BASE_API_URL}/Products`);
                
                if(result.data.length){
                    this.props.setProducts(result.data);
                }

            } catch (error) {
                console.error(error)
            }
            
        }
    }

    render(){
        return(
            <MaterialTable
                title="Products"
                columns={this.props.columns}
                data={this.props.productList}
                editable={{
                    onRowAdd: (newData) =>
                        new Promise(async (resolve) => {
                            const data = [...this.props.productList];
                            data.push(newData);
                            this.props.setProducts(data);
                            const result = await axios.post(`${BASE_API_URL}/Products/`,newData)
                            console.log(result)
                            resolve();
                        }),
                    onRowUpdate: (newData, oldData) =>
                    new Promise((resolve) => {
                        resolve();
                        if (oldData) {
                            const data = [...this.props.productList];
                            data[data.indexOf(oldData)] = newData;
                            this.props.setProducts(data);
                            axios.put(`${BASE_API_URL}/Products/${newData.id}`,newData)
                        }
                    }),
                    onRowDelete: (oldData) =>
                    new Promise((resolve) => {
                        resolve();
                        const data = [...this.props.productList];
                        data.splice(data.indexOf(oldData), 1);
                        this.props.setProducts(data);
                        axios.put(`${BASE_API_URL}/Products/${oldData.id}`)
                    }),
                }}
            />
        )
    }

}

function mapStateToProps(reducers)  {
	return reducers.productReducer;
}

export default connect(mapStateToProps, productActions)(Products);