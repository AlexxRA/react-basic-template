import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import { createStore, applyMiddleware } from 'redux';
import reducers from './redux/reducers/index.js';
import reduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import Products from './containers/Products'

const store = createStore(
	reducers, /* Reducers */
	applyMiddleware(reduxThunk), /* Initial state */
);

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route
            path={'/products'}
            exact
            render={(props) => <Products {...props}/>}
          >

          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
